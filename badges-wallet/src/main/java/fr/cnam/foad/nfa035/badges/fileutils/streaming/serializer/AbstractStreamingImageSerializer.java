package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * Classe abstraite visant à structurer/guider le développement dee manière rigoureuse
 *
 * @param <S>
 * @param <M> Le Media de sérialisation,
 *           à vers lequel nous voulons sérialiser notre image en base 64
 */
public abstract class AbstractStreamingImageSerializer<S,M> implements ImageStreamingSerializer<S,M> {

    /**
     * Sérialise une image depuis un support quelconque vers un media quelconque
     *
     * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public void serialize(S source, M media) throws IOException {
        try(OutputStream os = getSerializingStream(media)){
            getSourceInputStream(source).transferTo(os);
        }
    }

    public abstract InputStream getSourceInputStream(File source) throws IOException;

    public abstract void serialize(File source, WalletFrameMedia media) throws IOException;
}
