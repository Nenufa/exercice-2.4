package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;
import java.util.Date;
import java.util.Objects;

/**
 * POJO model représentant le Badge Digital
 */
public class DigitalBadge {

    private DigitalBadgeMetadata metadata;
    private File badge;
    private Date begin;
    private Date end;
    private String serial;

    /**
     * Constructeur par défaut
     */
    public DigitalBadge(){};

    /**
     * Constructeur
     *
     * @param metadata
     * @param badge
     */
    public DigitalBadge(DigitalBadgeMetadata metadata, File badge) {
        this.metadata = metadata;
        this.badge = badge;
    }

    /**
     * Constructeur
     *
     * @param metadata
     * @param badge
     * @param begin
     * @param end
     * @param serial
     */
    public DigitalBadge(DigitalBadgeMetadata metadata, File badge, Date begin, Date end, String serial) {
        this.metadata = metadata;
        this.badge = badge;
        this.begin = begin;
        this.end = end;
        this.serial = serial;
    }

    /**
     * Getter des métadonnées du badge
     * @return les métadonnées DigitalBadgeMetadata
     */
    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }

    /**
     * Setter des métadonnées du badge
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * Getter du badge (l'image)
     * @return le badge (File)
     */
    public File getBadge() {
        return badge;
    }

    /**
     * Setter du badge (Fichier image)
     * @param badge
     */
    public void setBadge(File badge) {
        this.badge = badge;
    }

    /**
     * {@inheritDoc}
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadge that = (DigitalBadge) o;
        return metadata.equals(that.metadata) && badge.equals(that.badge) && begin.equals(that.begin) && end.equals(that.end) && serial.equals(that.serial);
    }

    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(metadata, badge, begin, end, serial);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadge{" +
                "metadata=" + metadata +
                ", badge=" + badge +
                ", begin=" + begin +
                ", end=" + end +
                ", serial=" + serial +
                '}';
    }

    /**
     * Getter de la date de début
     * @return begin
     */
    public Date getBegin() {
        return begin;
    }

    /**
     * Setter de la date de début
     * @param begin
     */
    public void setBegin(Date begin) {
        this.begin = begin;
    }

    /**
     * Getter de la date de fin
     * @return end
     */
    public Date getEnd() {
        return end;
    }

    /**
     * Setter de la date de fin
     * @param end
     */
    public void setEnd(Date end) {
        this.end = end;
    }

    /**
     * Getter du serial de l'image
     * @return serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * Setter du serial de l'image
     * @param serial
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }
}
